
;;;; ================================================
;;;; = 3I020 - Principes des programmes déclaratifs =
;;;; ================================================
;;;; = TME Solo - édition 2016/2017                 =
;;;; ================================================

(ns tme-solo.tme1
  (:use midje.sweet))

;;; DUREE : 45 minutes

;;; IMPORTANT
;;; - les trois exercices du TME sont indépendants
;;; - pendant le TME il est interdit de communiquer, par quelque moyen que ce soit
;;;   avec d'autres étudiants
;;; - tous les supports de cours sont autorisés, de même que l'accès aux documents
;;;   clojure en ligne.

;;; Exercice 1 :
;;; ==========

;;; Compléter la définition de fonction suivante :

(defn mdiff
  "Retourne le *diff* des deux maps `m1` et `m2` sous
la forme d'un vecteur composé de trois maps:
  1. la map des clés/valeurs uniquement présentes dans `m1`
  2. la map des clés/valeurs uniquement présentes dans `m2`
  3. la map des clés/valeurs communes."
  [m1 m2]
  (loop [m1 m1, m2 m2, res1 {}, res2 {}, meme {}]
    (if (seq m1)
      (let [[k, v1] (first m1)
            v2 (get m2 k ::not-found)]
        (if (= v2 ::not-found)
          (recur (rest m1) m2 (assoc res1 k v1) res2 meme)
          (if (= v1 v2)
            (recur (rest m1) (dissoc m2 k) res1 res2 (assoc meme k v1))
            (recur (rest m1) m2 (assoc res1 k v1) (assoc res2 k v2) meme))))
      [res1, (merge m2 res2), meme])))

;;; afin de valider les tests ci-dessous :

(fact "à propose de `mdiff`."

      (mdiff {:a 1, :b 2, :c 3}
             {:b 5, :a 1, :d 4})
      => [{:b 2, :c 3} {:b 5, :d 4} {:a 1}]

      (mdiff {:a 1, :b 2, :c 3}
             {:a 3, :b 5, :c 5})
      => [{:a 1, :b 2, :c 3} {:a 3, :b 5, :c 5} {}]

      (mdiff {:a 1, :b 2, :c 3}
             {:c 3, :b (+ 1 1), :a 1})
      => [{} {} {:c 3, :b 2, :a 1}]

      ;; attention à ce test
      (mdiff {:a 1, nil 2, :b nil}
             {:a 1, nil 2, :b 2})
      => [{:b nil} {:b 2} {:a 1, nil 2}])


;;; Exercice 2
;;; ==========

;;; Compléter la définition de fonction suivante :

(defn longest
  "Retourne le plus long sous-vecteurs croissant (au sens large)
 de `v`. 
Remarque 1 : si deux sous-vecteurs sont de même longueur, on retourne
  celui avec les indices les plus grands.
Remarque 2 : le sous-vecteur retourné doit être de taille au moins 2,
 et on retourne `nil` s'il n'y pas de sous-vecteur croissant dans `v`."
  ([v] (if (>= (count v) 2)
         (longest (first v) #{(first v)} #{(first v)} (rest v) 1 1 1 1)
         nil))
  ([i sousvect sousvectMax restvect lg lgMax cpt cptMax] (if (seq restvect)
                                                           (if (< i (first restvect))
                                                             (longest (first restvect) (conj sousvect (first restvect)) sousvectMax (rest restvect) (inc lg) lgMax (inc cpt) cptMax)
                                                             (if (and (>= lg lgMax) (>= cpt cptMax))
                                                               (longest (first restvect) #{(first restvect)} sousvect (rest restvect) 1 lg (inc cpt) cpt)
                                                               (longest (first restvect) #{(first restvect)} sousvectMax (rest restvect) 1 lgMax (inc cpt) cptMax)))
                                                           (if (>= lgMax 2)
                                                             (vec (apply sorted-set sousvectMax))
                                                             nil))))

;;; afin de valider les tests ci-dessous :

(fact "à propos de `longest`."
      (longest [1 3 5 2 4 1 5 7 8 4 9 7])
      => [1 5 7 8]

      (longest [1 2 3 4 2 3 4 5 1])
      => [2 3 4 5]

      (longest []) => nil

      (longest [5 4 3 2 1]) => nil)

;;; Exercice 3 :
;;; ==========

;;; On souhaite implémenter l'algorithme de compression RLE
;;; (run-length encoding) qui consiste à encoder une valeur v
;;; répétée n fois par un couple [v, n].

;;; On travaillera dans les séquences et on essaiera de garantir
;;; un maximum de paresse.

;;; Question 3.1 : algorithme de compression
;;; ------------

;;; Compléter la définition de fonction suivante :

(defn compress
  "Compresse de façon paresseuse la séquence `s` en entrée
  par l'algorithme RLE."
  ([s] (if (seq s)
         (compress (first s) 1 (rest s))
         ()))
  ([word nb s] (if (seq s)
                 (lazy-seq (if (= word (first s))
                             (compress word (inc nb) (rest s))
                             (cons [word nb] (compress (first s) 1 (rest s)))))
                 (list [word nb]))))

;;; afin de valider les tests ci-dessous :

(fact "à propos de `compress`."
      (compress [:a :a :b :b :b :a :a :a :c :d :e :e :e])
      => '([:a 2] [:b 3] [:a 3] [:c 1] [:d 1] [:e 3])

      (compress [:a :b :c]) => '([:a 1] [:b 1] [:c 1])

      (compress ()) => ()

      ;; attention : boucle infinie potentielle si compress n'est pas
      ;; paresseuse !
      (take 5 (compress (apply concat (map #(repeat % %) (range)))))
      => '([1 1] [2 2] [3 3] [4 4] [5 5]))


;;; Question 3.1 : algorithme de décompression
;;; ------------

;;; Compléter la définition de fonction suivante :

(defn decompress
  "Décompresse la séquence `s` codée en RLE en entrée."
  ([s] (if (seq s)
         (decompress (second (first s)) (ffirst s) (rest s))
         ()))
  ([nb word sq] (if (seq sq)
                  (if (= nb 0)
                    (lazy-seq (decompress (second (first sq)) (ffirst sq) (rest sq)))
                    (lazy-seq (cons word (decompress (dec nb) word sq))))
                  (if (> nb 0)
                    (lazy-seq (cons word (decompress (dec nb) word sq)))
                    ()))))

(fact "à propos de `decompress`."
      (decompress '([:a 2] [:b 4] [:c 1] [:d 3]))
      => '(:a :a :b :b :b :b :c :d :d :d)

      (let [v [:a :a :b :b :b :a :a :a :c :d :e :e :e]]
        (= v (decompress (compress v)))) => true

      (decompress '([:a 3] [:b 0] [:c 2])) => '(:a :a :a :c :c)

      ;; attention : boucle infinie potentielle si decompress
      ;; n'est pas paresseuse !
      (take 10 (decompress (map (fn [n] [n n]) (rest (range)))))
      => '(1 2 2 3 3 3 4 4 4 4))






